import requests
import csv

astros_json = requests.get("http://api.open-notify.org/astros.json").json()
astros = astros_json["people"]

ASTROS_KEYS = ["name", "craft"]


def write_to_csv(filename: str, data: dict, keys: list[str]) -> None:
    with open(filename, "w") as f:
        people_writer = csv.DictWriter(f, keys)
        people_writer.writeheader()
        people_writer.writerows(data)


# Writes all astros to people.csv
write_to_csv("people.csv", astros, ASTROS_KEYS)

# Groups all astros by craft value, ex: {"ISS": {...}, "Tiangong": {...}}
astros_grouped_by_craft = {}
for astro in astros:
    if astro["craft"] not in astros_grouped_by_craft:
        astros_grouped_by_craft[astro["craft"]] = []

    astros_grouped_by_craft[astro["craft"]].append(astro)

# Writes all astros by craft value to [craft_value].csv
for keyname in astros_grouped_by_craft:
    write_to_csv(
        f"{keyname}.csv", astros_grouped_by_craft[keyname], ASTROS_KEYS
    )
