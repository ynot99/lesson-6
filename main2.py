import sqlite3

from utils import (
    generate_people_values,
    cond_people_print_sql_data,
    get_random_email_domain,
)


# region FIRST PART
# region 1
"""
1. создаст базу данных (произвольное имя)
"""

conn = sqlite3.connect("homework6.db")

cursor = conn.cursor()
# endregion 1


# region 2
"""
2. Создаст таблицу people с атрибутами 
id (обязательно), 
имя (обязательно), 
фамилия (обязательно), 
пол (обязательно), 
зарплата, 
должность, 
email, 
возраст (обязательно)
"""

# FOREIGN KEY support
cursor.execute("PRAGMA foreign_keys = ON;")

# enum for `gender` field in `people` table
cursor.execute(
    """
    CREATE TABLE IF NOT EXISTS gender (
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        name VARCHAR(13) UNIQUE
    );
    """
)
gender_count = cursor.execute("SELECT COUNT(*) FROM gender;").fetchone()[0]
if not gender_count:
    cursor.execute(
        """
        INSERT INTO gender (name) VALUES
        ('not set'),
        ('male'),
        ('female'),
        ('not specified');
        """
    )
del gender_count

cursor.execute(
    """
    CREATE TABLE IF NOT EXISTS people (
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
        first_name VARCHAR(20) NOT NULL,
        last_name VARCHAR(20) NOT NULL,
        gender INTEGER NOT NULL,
        salary UNSIGNED INT,
        position VARCHAR(255),
        email VARCHAR(70) UNIQUE,
        age UNSIGNED INT NOT NULL,
        FOREIGN KEY (gender)
        REFERENCES gender (id)
    );
    """
)
# endregion 2


# region 3
"""
3. Сгенерирует 20 записей на ваш вкус 
причем для тех полей которые не обязательны указывать данные выборочно 
например с 20 только 5 имеют email, 10 должность - и так далее 
а зарплата и возраст генерируется с помощью функции рандом в пайтон
"""

cursor.executemany(
    """
    INSERT INTO people (first_name, last_name, gender, salary, position, email, age) VALUES
    (?, ?, ?, ?, ?, ?, ?);
    """,
    generate_people_values(),
)

cond_people_print_sql_data(
    conn,
    "After insertion 20 people to database",
    # do_print=False,
)
# endregion 3


# region 4
"""
4. Добавить две записи для Laurence Wachowski и Andrew Wachowski 
пол мужчина другие данные произвольные
"""

cursor.execute(
    """
    INSERT INTO people (first_name, last_name, gender, salary, position, email, age) VALUES
    ('Laurence', 'Wachowski', 2, 10000, 'Breeder', 'laurence-wachowski@hotmail.com', 26),
    ('Andrew', 'Wachowski', 2, 100000, 'Software Engineer', 'andrew-wachowski@gmail.com', 26);
    """
)

cond_people_print_sql_data(
    conn,
    "Laurence Wachowski and Andrew Wachowski joined the database",
    # do_print=False,
)
# endregion 4


# region 5
"""
5. Изменить пол для Laurence Wachowski и Andrew Wachowski на женщина
"""

cursor.execute(
    """
    UPDATE people 
    SET gender = 3
    WHERE last_name == 'Wachowski';
    """
)

cond_people_print_sql_data(
    conn,
    "Laurence Wachowski and Andrew Wachowski are females now",
    # do_print=False,
)
# endregion 5


# region 6
"""
6. Изменить имя Laurence Wachowski на Lana
"""

cursor.execute(
    """
    UPDATE people
    SET first_name = 'Lana'
    WHERE first_name == 'Laurence' AND
    last_name == 'Wachowski';
    """
)

cond_people_print_sql_data(
    conn,
    "Laurence Wachowski are now Lana Wachowski",
    # do_print=False,
)
# endregion 6


# region 7
"""
7. Изменить имя Andrew Wachowski на Lilly
"""

cursor.execute(
    """
    UPDATE people
    SET first_name = 'Lilly'
    WHERE first_name == 'Andrew' AND
    last_name == 'Wachowski';
    """
)

cond_people_print_sql_data(
    conn,
    "Andrew Wachowski are now Lilly Wachowski",
    # do_print=False,
)
# endregion 7


# region 8
"""
8. добавить для всех записей в которых нет email - email 
который будет состоять из имени и фамилии и произвольный хост, 
то есть для человека Sarah Connor email должно быть sarah.connor@sky.net 
причем email должно генерироваться средствами sql пример:
INSERT INTO artist
SELECT "SQLite" || "CONCAT ';
FROM artists;
"""

cursor.execute(
    f"""
    UPDATE people
    SET email = 
    LOWER(first_name) || '.' 
    || LOWER(last_name) || id || '@' 
    || '{get_random_email_domain()}'
    WHERE email IS NULL;
    """
)

cond_people_print_sql_data(
    conn,
    "People learned how to use email",
    # do_print=False,
)
# endregion 8
# endregion FIRST PART


# region SECOND PART
# region 1
"""
1. Вывести всех людей с зарплатой больше 10000
"""

cond_people_print_sql_data(
    conn,
    "Salary greater than 10000",
    """
    SELECT
        people.id, 
        people.first_name, 
        people.last_name, 
        gender.name AS gender, 
        people.salary, 
        people.position, 
        people.email, 
        people.age 
    FROM people
    LEFT JOIN gender 
    ON people.gender == gender.id
    WHERE salary > 10000;
    """,
    # do_print=False,
)
# endregion 1


# region 2
"""
2. Вывести всех людей с зарплатой 10000 и старше 25
"""

cond_people_print_sql_data(
    conn,
    "Salary equals 10000 and age greater than 25",
    """
    SELECT 
        people.id, 
        people.first_name, 
        people.last_name, 
        gender.name AS gender, 
        people.salary, 
        people.position, 
        people.email, 
        people.age
    FROM people
    LEFT JOIN gender 
    ON people.gender == gender.id
    WHERE
    (salary == 10000) AND (age > 25);
    """,
    # do_print=False,
)
# endregion 2


# region 3
"""
3. удалить всех людей без должности
"""

cursor.execute(
    """
    DELETE FROM people
    WHERE position IS NULL;
    """
)

cond_people_print_sql_data(
    conn,
    "Global reduction from... their houses?",
    # do_print=False,
)
# endregion 3
# endregion SECOND PART

# I do not want to store data in this program, because of UNIQUE email
# conn.commit()

cursor.close()
conn.close()
