from random import randint, choice
from sqlite3.dbapi2 import Connection
import pandas
import inspect

from people_data import (
    get_email_domains,
    get_first_names,
    get_last_names,
    get_positions,
)


def get_random_email_domain() -> str:
    return choice(get_email_domains())


# [GENERATOR] generates 20 values for db
def generate_people_values() -> tuple:
    first_names = get_first_names()
    last_names = get_last_names()
    positions = get_positions()
    email_domains = get_email_domains()

    for i in range(20):
        first_name, last_name = (
            choice(first_names),
            choice(last_names),
        )
        # 'NULL' for unique fields throws error, so I used 'None' for empty value
        gender, salary, position, email, age = (
            randint(1, 4),
            randint(4000, 14000),
            choice(positions) if i % 2 == 0 else None,
            # I added 'i' variable to email to remove collision on UNIQUE value, provided that we do not store this data
            f"{str.lower(first_name)}-{str.lower(last_name)}{i}@{choice(email_domains)}"
            if i % 4 == 0
            else None,
            randint(1, 100),
        )
        yield (
            first_name,
            last_name,
            gender,
            salary,
            position,
            email,
            age,
        )


# for debugging
# Do not forget ; for sql query
def cond_people_print_sql_data(
    connection: Connection,
    header: str = "All people data",
    sql: str = """SELECT 
        people.id, 
        people.first_name, 
        people.last_name, 
        gender.name AS gender, 
        people.salary, 
        people.position, 
        people.email, 
        people.age
        FROM people 
        LEFT JOIN gender 
        ON people.gender == gender.id;
    """,
    do_print: bool = True,
) -> None:
    if do_print:
        line_number = inspect.currentframe().f_back.f_lineno
        print(
            f"\n\033[96m---\t{header}\t---\tCalled from {line_number} line\033[0m\n\n",
            pandas.read_sql_query(
                sql,
                connection,
            ).to_string(),
        )
