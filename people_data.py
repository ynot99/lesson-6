# https://www.ssa.gov/oact/babynames/decades/century.html
def get_first_names() -> list[str]:
    return [
        "Nobuo",
        "Leroy",
        "James",
        "Robert",
        "Michael",
        "Joseph",
        "Thomas",
        "Christopher",
        "Claire",
        "Nina",
        "Anna",
        "Joshua",
        "Edward",
        "Scott",
        "Julia",
        "Russell",
        "Rachel",
        "Jean",
        "Alice",
        "Vincent",
    ]


# https://www.familyeducation.com/baby-names/browse-names/surname
def get_last_names() -> list[str]:
    return [
        "Uematsu",
        "Smith",
        "Redfield",
        "Williams",
        "Brown",
        "Frost",
        "Garcia",
        "Miller",
        "Davis",
        "Rodriguez",
        "Martinez",
        "Eagle",
        "Ebaugh",
        "La",
        "Labiche",
        "Quackenbush",
        "U",
        "Uber",
        "Zagami",
        "Zen",
    ]


# https://zety.com/blog/job-titles
def get_positions() -> list[str]:
    return [
        "Marketing Coordinator",
        "Medical Assistant",
        "Web Designer",
        "Dog Trainer",
        "President of Sales",
        "Nursing Assistant",
        "Project Manager",
        "Librarian",
        "Project Manager",
        "Account Executive",
    ]


# https://email-verify.my-addr.com/list-of-most-popular-email-domains.php
def get_email_domains() -> list[str]:
    return [
        "gmail.com",
        "yahoo.com",
        "hotmail.com",
        "aol.com",
        "msn.com",
        "live.com",
        "rediffmail.com",
        "ymail.com",
        "outlook.com",
        "googlemail.com",
        "rocketmail.com",
        "ntlworld.com",
        "juno.com",
        "mac.com",
        "aim.com",
        # who will be that lucky man?
        "yandex.ru",
    ]
